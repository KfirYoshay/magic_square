#pragma once
#define N (4)

/*
@brief - This function will receive matrix N*N, and will check if the matrix is a magic square
@Param-in - matrix N*N
@Param-in - matrix size
@Return value - The function will return 1 for magic square, 0 otherwise
*/
int is_matrix_magic_square(int magic_sqaure[N][N], int size);