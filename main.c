#include <stdio.h>
#include "magic_square.h"


void print_matrix(int matrix[N][N], int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("[ ");
		for (int j = 0; j < size; j++)
		{
			printf("%d ", matrix[i][j]);
		}
		printf("]\n");
	}
}

int main(void)
{
	int matrix[N][N] = { {7,14,4,9},{15,6,12,1},{2,3,13,16},{10,11,5,8} };
	print_matrix(matrix, N);

	if (0 == is_matrix_magic_square(matrix, N))
	{
		printf("\n\nThe Matrix is not a Magic Square!");
		return -1;
	}
	printf("\n\nThe Matrix is a Magic Square!");
	return 0;
}