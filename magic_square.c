#include "magic_square.h"

/*
@brief - This function will receive matrix N*N, and row_index to sum and return the sum of the row
@Param-in - matrix N*N
@Param-in - matrix size
@Param-in - row_index to sum
@Return value - sum of the row
*/
int sum_matrix_row(int magic_sqaure[N][N], int size, int row_index)
{
	int sum_row = 0;
	for (int i = 0; i < size; i++)
	{
		sum_row += magic_sqaure[row_index][i];
	}
	return sum_row;
}

/*
@brief - This function will receive matrix N*N, and column_index to sum and return the sum of the column
@Param-in - matrix N*N
@Param-in - matrix size
@Param-in - column_index to sum
@Return value - sum of the column
*/
int sum_matrix_column(int magic_sqaure[N][N], int size, int col_index)
{
	int sum_column = 0;
	for (int i = 0; i < size; i++)
	{
		sum_column += magic_sqaure[i][col_index];
	}
	return sum_column;
}

/*
@brief - This function will receive matrix N*N, and will return the sum of its main diagonal
@Param-in - matrix N*N
@Param-in - matrix size
@Return value - sum of main diagonal
*/
int sum_matrix_main_diagonal(int magic_sqaure[N][N], int size)
{
	int main_diagonal_sum = 0;
	for (int i = 0;i < size; i++)
	{
		main_diagonal_sum += magic_sqaure[i][i];
	}
	return main_diagonal_sum;
}

/*
@brief - This function will receive matrix N*N, and will return the sum of its secondary diagonal
@Param-in - matrix N*N
@Param-in - matrix size
@Return value - sum of secondary diagonal
*/
int sum_matrix_second_diagonal(int magic_sqaure[N][N], int size)
{
	int second_diagonal_sum = 0;
	for (int i = 0;i < size; i++)
	{
		second_diagonal_sum += magic_sqaure[i][size - i - 1];
	}
	return second_diagonal_sum;
}

/*
@brief - This function will compare the sum to the rows and columns sum
@Param-in - sum to compare
@Param-in - matrix N*N
@Param-in - matrix size
@Return value - 0 if not equal, 1 otherwise
*/
int compare_sum_to_rows_and_cols(int sum_to_compare, int magic_sqaure[N][N], int size)
{
	int row_sum = 0;
	int col_sum = 0;
	for (int i = 0;i < size;i++)
	{
		row_sum = sum_matrix_row(magic_sqaure, size, i);
		col_sum = sum_matrix_column(magic_sqaure, size, i);
		if (row_sum != sum_to_compare || col_sum != sum_to_compare)
		{
			return 0;
		}
	}
	return 1;
}

/*
@brief - This function will return the initial sum to compare with (main diagonal sum)
@Param-in - matrix N*N
@Param-in - matrix size
@Return value - sum of main diagonal
*/
int get_sum_to_compare(int magic_sqaure[N][N], int size)
{
	return sum_matrix_main_diagonal(magic_sqaure, size);
}

/*
@brief - This function will compare the sum to the secondary diagonal
@Param-in - sum to compare
@Param-in - matrix N*N
@Param-in - matrix size
@Return value - 0 if not equal, 1 otherwise
*/
int compare_sum_to_diagonal(int sum_to_compare, int magic_sqaure[N][N], int size)
{
	if (sum_to_compare != sum_matrix_second_diagonal(magic_sqaure, size))
	{
		return 0;
	}
	return 1;
}

int is_matrix_magic_square(int magic_sqaure[N][N], int size)
{
	int sum_to_compare = get_sum_to_compare(magic_sqaure,size);
	return compare_sum_to_diagonal(sum_to_compare, magic_sqaure, size) && compare_sum_to_rows_and_cols(sum_to_compare, magic_sqaure, size);
}